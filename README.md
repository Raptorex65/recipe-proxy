# recipe-proxy

Nginx proxy app for our recipe application

# Usage

Environment Variables

LISTEN_PORT - Port to listen on (default: 8000)

APP_HOST - Hostname of the app to forward requests to (default: app)

APP_PORT - Port of the app to forward requests to (default: 9000)

# Getting Started

To start project, run:

docker-compose up
